import { contextBridge, ipcRenderer } from "electron";
import { Command, Direction } from "./structures/keybinds";

contextBridge.exposeInMainWorld("electronAPI", {
    setIgnoreMouseEvents: (value: boolean) =>
        ipcRenderer.send("setIgnoreMouseEvents", value),
    handleCommand: (
        callback: (event: any, command: Command, direction?: Direction) => any
    ) => ipcRenderer.on("command", callback),
});
