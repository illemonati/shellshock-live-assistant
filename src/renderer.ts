import Mousetrap from "mousetrap";
import "./index.css";
import { Command, Direction } from "./structures/keybinds";
import { Point } from "./structures/point";

const main = () => {
    const mainCanvas: HTMLCanvasElement | undefined =
        document.querySelector(".mainCanvas");

    const selfLabel: HTMLParagraphElement | undefined =
        document.querySelector(".self");

    const powerLabel: HTMLParagraphElement | undefined =
        document.querySelector(".power");

    const angleLabel: HTMLParagraphElement | undefined =
        document.querySelector(".angle");

    const windLabel: HTMLParagraphElement | undefined =
        document.querySelector(".wind");

    if (!(mainCanvas && powerLabel && angleLabel && windLabel)) {
        return;
    }
    const handler = new ArcHandler(
        mainCanvas,
        selfLabel,
        powerLabel,
        angleLabel,
        windLabel
    );
    handler.start();
};

class ArcHandler {
    canvas: HTMLCanvasElement;
    selfLabel: HTMLParagraphElement;
    powerLabel: HTMLParagraphElement;
    angleLabel: HTMLParagraphElement;
    windLabel: HTMLParagraphElement;
    ctx: CanvasRenderingContext2D;
    selfPosition: Point | undefined;
    angle: number;
    power: number;
    wind: number;

    constructor(
        canvas: HTMLCanvasElement,
        selfLabel: HTMLParagraphElement,
        powerLabel: HTMLParagraphElement,
        angleLabel: HTMLParagraphElement,
        windLabel: HTMLParagraphElement
    ) {
        this.canvas = canvas;
        this.ctx = this.canvas.getContext("2d");

        this.selfLabel = selfLabel;
        this.powerLabel = powerLabel;
        this.angleLabel = angleLabel;
        this.windLabel = windLabel;

        this.angle = 60;
        this.power = 100;
        this.wind = 0;
        this.ctx.fillStyle = "cyan";
    }

    adjustCanvasSize() {
        const bounds = this.canvas.getBoundingClientRect();
        this.canvas.width = bounds.width;
        this.canvas.height = bounds.height;
    }

    redraw() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawSelf();
        this.drawArc();
    }

    drawArc() {
        if (!this.selfPosition) return;
        this.ctx.beginPath();
        this.ctx.strokeStyle = "cyan";

        const velocity = this.power / 100;
        const radians = this.angle * (Math.PI / 180);
        const velocityX = velocity * Math.cos(radians);
        const velocityY = velocity * Math.sin(radians);
        const windAcceleration = (45 / 1561784 / 53) * this.wind;
        const wScale = this.canvas.width / 1792;
        const accelerationY = -0.000427715996578;
        let time = 0;
        const timeIncrement = 1;

        const currentPoint = Object.assign(
            Object.create(Point),
            this.selfPosition
        );

        this.ctx.moveTo(currentPoint.x, currentPoint.y);

        while (
            currentPoint.x < this.canvas.width &&
            currentPoint.y < this.canvas.height
        ) {
            time += timeIncrement;
            currentPoint.x =
                (velocityX * time + 0.5 * (windAcceleration * time ** 2)) *
                    wScale +
                this.selfPosition.x;
            currentPoint.y =
                -(velocityY * time + 0.5 * (accelerationY * time ** 2)) *
                    wScale +
                this.selfPosition.y;

            this.ctx.lineTo(currentPoint.x, currentPoint.y);
        }
        this.ctx.stroke();
    }

    drawSelf() {
        if (!this.selfPosition) return;
        this.ctx.beginPath();
        this.ctx.arc(
            this.selfPosition.x,
            this.selfPosition.y,
            10,
            0,
            2 * Math.PI
        );
        this.ctx.fillStyle = "cyan";
        this.ctx.fill();
    }

    fillAngleLabel() {
        const displayAngle = this.angle > 90 ? -(180 - this.angle) : this.angle;
        this.angleLabel.innerText = displayAngle.toString();
    }

    adjustAim(direction: Direction) {
        switch (direction) {
            case Direction.Up:
                this.power += 1;
                this.powerLabel.innerText = this.power.toString();
                break;
            case Direction.Down:
                this.power -= 1;
                this.powerLabel.innerText = this.power.toString();
                break;
            case Direction.Left:
                this.angle += 1;
                this.fillAngleLabel();
                break;
            case Direction.Right:
                this.angle -= 1;
                this.fillAngleLabel();
                break;
            case Direction.WindLeft:
                this.wind -= 1;
                this.windLabel.innerText = this.wind.toString();
                break;
            case Direction.WindRight:
                this.wind += 1;
                this.windLabel.innerText = this.wind.toString();
                break;
        }
        this.redraw();
    }

    getSelfPosition() {
        console.log("getSelfPosition");
        const handleClick = (e: MouseEvent) => {
            window.electronAPI.setIgnoreMouseEvents(true);
            const bounds = this.canvas.getBoundingClientRect();
            this.selfPosition = new Point(
                e.clientX - bounds.left,
                e.clientY - bounds.top
            );
            this.selfLabel.innerText = this.selfPosition.toString();
            this.redraw();
            this.canvas.removeEventListener("mousedown", handleClick);
        };
        this.canvas.addEventListener("mousedown", handleClick);
        window.electronAPI.setIgnoreMouseEvents(false);
    }

    start() {
        this.adjustCanvasSize();
        window.addEventListener("resize", () => this.adjustCanvasSize());

        window.electronAPI.handleCommand(
            (event: any, command: Command, direction?: Direction) => {
                console.log(event, direction, command);
                if (command === Command.AdjustAim) {
                    this.adjustAim(direction);
                } else if (command === Command.GetSelfPosition) {
                    this.getSelfPosition();
                }
            }
        );
        // Mousetrap.bind("ctrl+shift+s", () => this.getSelfPosition());
        // Mousetrap.bind("ctrl+shift+left", () => this.adjustAim(Direction.Left));
        // Mousetrap.bind("ctrl+shift+right", () =>
        //     this.adjustAim(Direction.Right)
        // );
        // Mousetrap.bind("ctrl+shift+up", () => this.adjustAim(Direction.Up));
        // Mousetrap.bind("ctrl+shift+down", () => this.adjustAim(Direction.Down));
    }
}

main();
