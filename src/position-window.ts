import { BrowserWindow } from "electron";
import { windowManager, Window } from "node-window-manager";

export class WindowPositionManager {
    targetWindow?: Window;
    overlayWindow: BrowserWindow;

    constructor(overlayWindow: BrowserWindow) {
        this.overlayWindow = overlayWindow;
        this.targetWindow = WindowPositionManager.getTargetWindow();
    }

    start() {
        setInterval(() => this.positionOverlayWindow(), 50);
    }

    positionOverlayWindow() {
        const visible = this.targetWindow.isVisible();

        if (!visible) {
            if (this.overlayWindow.isVisible()) {
                this.overlayWindow.hide();
            }
            return;
        } else {
            if (!this.overlayWindow.isVisible()) {
                this.overlayWindow.show();
            }
        }

        const targetBounds = this.getTargetWindowBounds();
        if (!targetBounds) return;
        this.overlayWindow.setBounds(targetBounds);
    }

    getTargetWindowBounds() {
        return this.targetWindow?.getBounds();
    }

    static getTargetWindow() {
        const windows = windowManager.getWindows();
        const targetWindow = windows.find(
            (window) => window.getTitle() === "ShellShock Live"
        );
        return targetWindow;
    }
}
