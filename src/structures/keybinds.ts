export enum Direction {
    Left,
    Right,
    Up,
    Down,
    WindLeft,
    WindRight,
}

export enum Command {
    AdjustAim,
    GetSelfPosition,
}
